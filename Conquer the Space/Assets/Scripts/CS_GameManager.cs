﻿using UnityEngine;
using System.Collections;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;

public class CS_GameManager : MonoBehaviour {
	public string Server_IP = "172.20.126.22";
	public int Server_Port = 9933;
	public string ZoneName = "BasicExamples2";
	public string UserName = "";
	public string RoomName = "The Lobby";

	//Current Player
	CS_User user = new CS_User ();
	//Users List
	ArrayList CS_Users = new ArrayList ();
	//Server
	SmartFox sfs;
	// Use this for initialization
	void Start () {
		sfs = new SmartFox ();
		sfs.ThreadSafeMode = true;

		sfs.AddEventListener (SFSEvent.CONNECTION, OnConnection);
		sfs.AddEventListener (SFSEvent.LOGIN, OnLogin);
		sfs.AddEventListener (SFSEvent.LOGIN_ERROR, OnLoginError);
		sfs.AddEventListener (SFSEvent.ROOM_JOIN, OnJoinRoom);
		sfs.AddEventListener (SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
		sfs.AddEventListener (SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

		sfs.Connect (Server_IP, Server_Port);
	}

	void OnJoinRoom(BaseEvent e){
		Debug.Log ("Joined Room: " + e.Params ["room"]);
		Room temp = (Room)e.Params["room"];
		Debug.Log (temp.Id);

		user.User_ID = temp.Id;

		ISFSObject objOut = new SFSObject ();
		objOut.PutClass ("currentPlayer", user);
		sfs.Send (new ExtensionRequest ("SetLocation", objOut));
		Debug.Log ("Send to the Server");
	}

	void OnJoinRoomError(BaseEvent e){
		Debug.Log ("Join Room Error (" + e.Params ["errorCode"] + "): " + e.Params ["errorMessage"]);
	}

	void OnLogin(BaseEvent e){
		Debug.Log ("Logged In: "+e.Params["user"] + e.Params["zone"]);
		sfs.Send (new JoinRoomRequest (RoomName));
	}

	void OnLoginError(BaseEvent e){
		int errorCode = (int)e.Params ["errorCode"];
		Debug.Log ("Login failed: "+e.Params["errorMessage"]);
	}
	
	void OnExtensionResponse(BaseEvent e){
		Debug.Log ("Server Response");
		string cmd = (string)e.Params ["cmd"];
		ISFSObject objIn = (SFSObject)e.Params ["params"];

		if (cmd == "SetLocation") {
			Debug.Log ("SetLocation");
			CS_Users = (ArrayList) objIn.GetClass("currentPlayers");
			Debug.Log(CS_Users.ToString());
		}

	}

	void OnConnection(BaseEvent e){
		if ((bool)e.Params ["success"]) {
			Debug.Log ("Connection Success");
			sfs.Send(new LoginRequest(UserName, "", ZoneName));
		} else {
			Debug.Log("Failed Connect");		
		}
	}
	// Update is called once per frame
	void Update () {
		sfs.ProcessEvents ();

	}
	
	void OnApplicationQuit(){
		if (sfs.IsConnected)
			sfs.Disconnect ();
	}
}
